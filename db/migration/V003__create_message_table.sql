DROP TABLE IF EXISTS "message";
DROP SEQUENCE IF EXISTS message_id_seq;

CREATE SEQUENCE message_id_seq;
ALTER TABLE message_id_seq
    OWNER TO admin;
GRANT ALL ON SEQUENCE message_id_seq TO admin;

CREATE TABLE "message"
(
    id                  BIGINT                          NOT NULL    DEFAULT nextval('message_id_seq'  :: REGCLASS),
    from_subscriber     BIGINT                          NOT NULL,
    to_subscriber       BIGINT                          NOT NULL,
    value               CHARACTER VARYING(1000),
    start_time          TIMESTAMP WITHOUT TIME ZONE     NOT NULL,
    cost                DOUBLE PRECISION                NOT NULL,
    CONSTRAINT message_id_pkey PRIMARY KEY (id),
    CONSTRAINT message_fromsubscriber_id_fk FOREIGN KEY (from_subscriber) REFERENCES subscriber (id)
        MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT message_tosubscriber_id_fk FOREIGN KEY (to_subscriber) REFERENCES subscriber (id)
        MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE message
    OWNER TO admin;

GRANT ALL ON TABLE message TO admin;
GRANT SELECT, UPDATE, INSERT, DELETE, REFERENCES ON TABLE message TO admin;