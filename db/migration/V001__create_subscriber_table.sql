DROP TABLE IF EXISTS "subscriber";
DROP SEQUENCE IF EXISTS subscriber_id_seq;

CREATE SEQUENCE subscriber_id_seq;
ALTER TABLE subscriber_id_seq
    OWNER TO admin;
GRANT ALL ON SEQUENCE subscriber_id_seq TO admin;

CREATE TABLE "subscriber"
(
    id          BIGINT                      NOT NULL    DEFAULT nextval('subscriber_id_seq' :: REGCLASS),
    name        CHARACTER VARYING(100)      NOT NULL,
    surname     CHARACTER VARYING(100),
    phone       CHARACTER VARYING(15),
    balance     DOUBLE PRECISION            NOT NULL    DEFAULT 0,
    status      CHARACTER VARYING(20)       NOT NULL,
    CONSTRAINT subscriber_pkey PRIMARY KEY (id)
);

ALTER TABLE subscriber OWNER TO admin;

GRANT ALL ON TABLE subscriber TO admin;
GRANT SELECT, UPDATE, INSERT, DELETE, REFERENCES ON TABLE subscriber TO admin;