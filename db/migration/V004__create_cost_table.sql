DROP TABLE IF EXISTS "cost";
DROP SEQUENCE IF EXISTS cost_id_seq;

CREATE SEQUENCE cost_id_seq;
ALTER TABLE cost_id_seq
    OWNER TO admin;
GRANT ALL ON SEQUENCE cost_id_seq TO admin;

CREATE TABLE "cost"
(
    id                  BIGINT                          NOT NULL    DEFAULT nextval('cost_id_seq'  :: REGCLASS),
    service_type        CHARACTER VARYING(100)          NOT NULL,
    value               DOUBLE PRECISION                NOT NULL,
    CONSTRAINT message_pkey PRIMARY KEY (id)
);

ALTER TABLE cost
    OWNER TO admin;

GRANT ALL ON TABLE cost TO admin;
GRANT SELECT, UPDATE, INSERT, DELETE, REFERENCES ON TABLE cost TO admin;