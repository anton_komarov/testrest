DROP TABLE IF EXISTS "call";
DROP SEQUENCE IF EXISTS call_id_seq;

CREATE SEQUENCE call_id_seq;
ALTER TABLE call_id_seq
    OWNER TO admin;
GRANT ALL ON SEQUENCE call_id_seq TO admin;

CREATE TABLE "call"
(
    id                  BIGINT                          NOT NULL    DEFAULT nextval('call_id_seq'  :: REGCLASS),
    from_subscriber     BIGINT                          NOT NULL,
    to_subscriber       BIGINT                          NOT NULL,
    duration            BIGINT                          NOT NULL    DEFAULT 0,
    start_time          TIMESTAMP WITHOUT TIME ZONE     NOT NULL,
    cost                DOUBLE PRECISION                NOT NULL,
    CONSTRAINT calls_pkey PRIMARY KEY (id),
    CONSTRAINT call_fromsubscriber_id_fk FOREIGN KEY (from_subscriber) REFERENCES subscriber (id)
        MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT call_tosubscriber_id_fk FOREIGN KEY (to_subscriber) REFERENCES subscriber (id)
        MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE call
    OWNER TO admin;

GRANT ALL ON TABLE call TO admin;
GRANT SELECT, UPDATE, INSERT, DELETE, REFERENCES ON TABLE call TO admin;