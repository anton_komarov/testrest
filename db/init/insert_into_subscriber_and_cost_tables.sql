INSERT INTO cost(service_type, value) VALUES ('CALL', 0.5);
INSERT INTO cost(service_type, value) VALUES ('MESSAGE', 1.5);

INSERT INTO subscriber(name, surname, phone, balance, status) VALUES ('firstSubscriberName', 'firstSubscriberSurname',
                                                                       '123456789', 5.0, 'ACTIVE');
INSERT INTO subscriber(name, surname, phone, balance, status) VALUES ('secondSubscriberName', 'secondSubscriberSurname',
                                                                       '123456789', 12.0, 'ACTIVE');
INSERT INTO subscriber(name, surname, phone, balance, status) VALUES ('thirdSubscriberName', 'thirdSubscriberSurname',
                                                                       '123456789', 0.0, 'BLOCKED');