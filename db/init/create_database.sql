SET client_encoding = 'UTF8';

DROP USER IF EXISTS admin;

CREATE USER admin WITH PASSWORD 'admin';

DROP DATABASE IF EXISTS subscriber_lifecycle;

CREATE DATABASE subscriber_lifecycle
WITH OWNER = admin
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;

GRANT ALL ON DATABASE subscriber_lifecycle TO admin;