package ru.komarov.backend;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.komarov.backend.config.TestAppConfig;
import ru.komarov.backend.dto.CallDto;
import ru.komarov.backend.dto.MessageDto;
import ru.komarov.backend.entity.Call;
import ru.komarov.backend.entity.Cost;
import ru.komarov.backend.entity.Message;
import ru.komarov.backend.entity.Subscriber;
import ru.komarov.backend.entity.enums.ServiceType;
import ru.komarov.backend.entity.enums.SubscriberStatus;
import ru.komarov.backend.repository.CallRepository;
import ru.komarov.backend.repository.CostRepository;
import ru.komarov.backend.repository.MessageRepository;
import ru.komarov.backend.repository.SubscriberRepository;
import ru.komarov.backend.service.SettingsService;
import ru.komarov.backend.service.SubscriberService;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestAppConfig.class})
public class Tests {

    @Inject
    SubscriberService subscriberService;
    @Inject
    SubscriberRepository subscriberRepository;
    @Inject
    CostRepository costRepository;
    @Inject
    CallRepository callRepository;
    @Inject
    MessageRepository messageRepository;
    @Inject
    SettingsService settingsService;

    @Test
    public void doCallTest() {
        subscriberService.doCall(getCallDto(1L, 1L));
        subscriberService.doCall(getCallDto(1L, 2L));

        List<Call> calls = callRepository.findAll();
        Assert.assertEquals(2, calls.size());
    }

    @Test
    public void sendMessageTest() {
        subscriberService.sendMessage(getMessageDto(1L, 1L));
        subscriberService.sendMessage(getMessageDto(1L, 2L));

        List<Message> messages = messageRepository.findAll();
        Assert.assertEquals(2, messages.size());
    }

    @Test
    public void changeBalanceTest() {
        Long id = 1L;
        Subscriber subscriber = subscriberRepository.findById(id).orElse(null);
        Assert.assertNotNull(subscriber);
        double oldBalance = subscriberService.getBalance(id);
        subscriberService.addToBalance(id, 20.0);
        subscriber = subscriberRepository.findById(id).orElse(null);
        Assert.assertNotNull(subscriber);
        double newBalance = subscriber.getBalance();
        Assert.assertNotEquals(newBalance, oldBalance);

        oldBalance = newBalance;
        subscriberService.reduceBalance(id, 10.0);
        subscriber = subscriberRepository.findById(id).orElse(null);
        Assert.assertNotNull(subscriber);
        newBalance = subscriber.getBalance();
        Assert.assertNotEquals(oldBalance, newBalance);
    }

    @Test
    public void changeCallLimitTest() {
        int oldLimit = settingsService.getCallLimit();
        int newLimit = oldLimit + 5;
        settingsService.changeCallLimit(newLimit);
        Assert.assertEquals(newLimit, settingsService.getCallLimit());
    }

    @Before
    public void prepareData() {
        createCosts();
        createSubscribers();
    }

    private CallDto getCallDto(Long fromId, Long toId) {
        return CallDto.builder().fromSubscriberId(fromId).toSubscriberId(toId).duration(2L).build();
    }

    private MessageDto getMessageDto(Long fromId, Long toId) {
        return MessageDto.builder().fromSubscriberId(fromId).toSubscriberId(toId).value("Test message").build();
    }

    private void createSubscribers() {
        List<Subscriber> subscribers = Stream.of(
                new Subscriber(null, "TestSubscriber1", null, null, 50.0, SubscriberStatus.ACTIVE),
                new Subscriber(null, "TestSubscriber2", null, null, 20.0, SubscriberStatus.ACTIVE),
                new Subscriber(null, "TestSubscriber3", null, null, 0.0, SubscriberStatus.BLOCKED)
        ).collect(Collectors.toList());

        subscriberRepository.saveAll(subscribers);
    }

    private void createCosts() {
        Cost callCost = new Cost(ServiceType.CALL, 0.5);
        costRepository.save(callCost);
        Cost messageCost = new Cost(ServiceType.MESSAGE, 1.5);
        costRepository.save(messageCost);
    }
}
