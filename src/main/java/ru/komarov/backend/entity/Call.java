package ru.komarov.backend.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "call")
@NoArgsConstructor
@Data
public class Call extends Event {

    private static final String SEQUENCE_NAME = "call_id_seq";

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = SEQUENCE_NAME, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "DURATION")
    private Long duration;

    public Call(Subscriber fromSubscriber, Subscriber toSubscriber, Long duration, Double cost) {
        super(fromSubscriber, toSubscriber, cost);
        this.duration = duration;
    }
}
