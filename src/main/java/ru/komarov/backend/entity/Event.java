package ru.komarov.backend.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@MappedSuperclass
class Event implements Serializable {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FROM_SUBSCRIBER")
    private Subscriber fromSubscriber;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TO_SUBSCRIBER")
    private Subscriber toSubscriber;

    @NotNull
    @Column(name = "COST")
    private Double cost;

    @NotNull
    @Column(name = "START_TIME")
    private LocalDateTime startTime;

    Event(@NotNull Subscriber fromSubscriber, @NotNull Subscriber toSubscriber, @NotNull Double cost) {
        this.fromSubscriber = fromSubscriber;
        this.toSubscriber = toSubscriber;
        this.cost = cost;
        this.startTime = LocalDateTime.now();
    }
}
