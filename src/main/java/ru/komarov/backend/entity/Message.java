package ru.komarov.backend.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "message")
@NoArgsConstructor
@Data
public class Message extends Event {

    private static final String SEQUENCE_NAME = "message_id_seq";

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = SEQUENCE_NAME, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    private Long id;

    @Column(name = "VALUE")
    private String value;

    public Message(Subscriber fromSubscriber, Subscriber toSubscriber, String value, Double cost) {
        super(fromSubscriber, toSubscriber, cost);
        this.value = value;
    }
}
