package ru.komarov.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.komarov.backend.entity.enums.SubscriberStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "subscriber")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Subscriber implements Serializable {

    private static final String SEQUENCE_NAME = "subscriber_id_seq";

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = SEQUENCE_NAME, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "PHONE")
    private String phone;

    @NotNull
    @Column(name = "BALANCE")
    private Double balance;

    @NotNull
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private SubscriberStatus status;
}
