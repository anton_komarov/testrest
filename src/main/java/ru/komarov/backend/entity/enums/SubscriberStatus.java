package ru.komarov.backend.entity.enums;

public enum SubscriberStatus {
    ACTIVE,
    BLOCKED
}
