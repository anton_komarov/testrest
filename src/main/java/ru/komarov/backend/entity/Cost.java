package ru.komarov.backend.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.komarov.backend.entity.enums.ServiceType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cost")
@NoArgsConstructor
@Data
public class Cost {

    private static final String SEQUENCE_NAME = "cost_id_seq";

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = SEQUENCE_NAME, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "SERVICE_TYPE")
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;

    @NotNull
    @Column(name = "VALUE")
    private Double value;

    public Cost(@NotNull ServiceType serviceType, @NotNull Double value) {
        this.serviceType = serviceType;
        this.value = value;
    }
}
