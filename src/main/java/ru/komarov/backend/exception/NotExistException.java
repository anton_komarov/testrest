package ru.komarov.backend.exception;

import org.springframework.http.HttpStatus;

public class NotExistException extends RuntimeException {

    private static final String ERROR_MESSAGE = "It is not possible to complete the operation because subscriber with id='%d' does not exist";
    public static final HttpStatus ERROR_STATUS = HttpStatus.NOT_FOUND;

    public NotExistException() {
        super("It is not possible to complete the operation because subscriber does not exist");
    }

    public NotExistException(Long subscriberId) {
        super(String.format(ERROR_MESSAGE, subscriberId));
    }
}
