package ru.komarov.backend.exception;

import org.springframework.http.HttpStatus;

public class SubscriberBlockedException extends RuntimeException {

    private static final String ERROR_MESSAGE = "It is not possible to complete the operation because the subscriber with id='%d' is blocked";
    public static final HttpStatus ERROR_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    public SubscriberBlockedException() {
        super("It is not possible to complete the operation because the subscriber is blocked");
    }

    public SubscriberBlockedException(Long subscriberId) {
        super(String.format(ERROR_MESSAGE, subscriberId));
    }
}
