package ru.komarov.backend.converter;

import ru.komarov.backend.dto.CallDto;
import ru.komarov.backend.entity.Call;

public interface CallConverter {

    CallDto toDto(Call call);
}
