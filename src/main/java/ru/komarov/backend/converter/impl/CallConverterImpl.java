package ru.komarov.backend.converter.impl;

import org.springframework.stereotype.Component;
import ru.komarov.backend.converter.CallConverter;
import ru.komarov.backend.dto.CallDto;
import ru.komarov.backend.entity.Call;

@Component
public class CallConverterImpl implements CallConverter {

    @Override
    public CallDto toDto(Call call) {
        if (call != null) {
            return CallDto.builder()
                    .id(call.getId())
                    .fromSubscriberId(call.getFromSubscriber().getId())
                    .toSubscriberId(call.getToSubscriber().getId())
                    .duration(call.getDuration())
                    .cost(call.getCost())
                    .startTime(call.getStartTime())
                    .build();
        }
        return null;
    }
}
