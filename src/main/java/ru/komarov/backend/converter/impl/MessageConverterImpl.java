package ru.komarov.backend.converter.impl;

import org.springframework.stereotype.Component;
import ru.komarov.backend.converter.MessageConverter;
import ru.komarov.backend.dto.MessageDto;
import ru.komarov.backend.entity.Message;

@Component
public class MessageConverterImpl implements MessageConverter {

    @Override
    public MessageDto toDto(Message message) {
        if (message != null) {
            return MessageDto.builder()
                    .id(message.getId())
                    .fromSubscriberId(message.getFromSubscriber().getId())
                    .toSubscriberId(message.getToSubscriber().getId())
                    .value(message.getValue())
                    .cost(message.getCost())
                    .startTime(message.getStartTime())
                    .build();
        }
        return null;
    }
}
