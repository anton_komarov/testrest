package ru.komarov.backend.converter;

import ru.komarov.backend.dto.MessageDto;
import ru.komarov.backend.entity.Message;

public interface MessageConverter {

    MessageDto toDto(Message message);
}
