package ru.komarov.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.komarov.backend.converter.CallConverter;
import ru.komarov.backend.converter.MessageConverter;
import ru.komarov.backend.dto.CallDto;
import ru.komarov.backend.dto.MessageDto;
import ru.komarov.backend.service.SubscriberService;
import ru.komarov.backend.service.impl.SubscriberServiceImpl;

import javax.inject.Inject;

@Slf4j
@RestController
@RequestMapping("/subscribers_lifecycle")
public class SubscriberController {

    private final SubscriberService subscriberService;
    private final CallConverter callConverter;
    private final MessageConverter messageConverter;

    @Inject
    public SubscriberController(SubscriberServiceImpl subscriberService, CallConverter callConverter,
                                MessageConverter messageConverter) {
        this.subscriberService = subscriberService;
        this.callConverter = callConverter;
        this.messageConverter = messageConverter;
    }

    @PostMapping(value = "calls", consumes = MediaType.APPLICATION_JSON_VALUE)
    public CallDto doCall(@RequestBody CallDto callDto) {
        LOGGER.info("Call doCall() with params: call='{}'", callDto);
        return callConverter.toDto(subscriberService.doCall(callDto));
    }

    @PostMapping(value = "/messages", consumes = MediaType.APPLICATION_JSON_VALUE)
    public MessageDto sendMessage(@RequestBody MessageDto messageDto) {
        LOGGER.info("Call sendMessage() with params: message='{}'", messageDto);
        return messageConverter.toDto(subscriberService.sendMessage(messageDto));
    }

    @GetMapping(value = "/subscribers/{id}/balance")
    public Double getBalance(@PathVariable("id") Long id) {
        LOGGER.info("Call getBalance() with params: id='{}'", id);
        return subscriberService.getBalance(id);
    }

    @PutMapping(value = "/subscribers/{id}/balance", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Double addToBalance(@PathVariable("id") Long id, @RequestBody Double delta) {
        LOGGER.info("Call addToBalance() with params: id='{}', delta='{}'", id, delta);
        return subscriberService.addToBalance(id, delta);
    }
}
