package ru.komarov.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.komarov.backend.service.SettingsService;

import javax.inject.Inject;

@Slf4j
@RestController
@RequestMapping("/subscribers_lifecycle/settings")
public class SettingsController {

    private final SettingsService settingsService;

    @Inject
    public SettingsController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    @PutMapping(value = "/limit", consumes = MediaType.APPLICATION_JSON_VALUE)
    public int changeCallLimit(@RequestBody int newCallLimit) {
        LOGGER.info("Call changeCallLimit() with params: newCallLimit='{}'", newCallLimit);
        return settingsService.changeCallLimit(newCallLimit);
    }
}
