package ru.komarov.backend.service;

import ru.komarov.backend.dto.CallDto;
import ru.komarov.backend.dto.MessageDto;
import ru.komarov.backend.entity.Call;
import ru.komarov.backend.entity.Message;

public interface SubscriberService {

    double getBalance(Long id);

    Call doCall(CallDto callDto);

    Message sendMessage(MessageDto messageDto);

    double addToBalance(Long subscriberId, Double delta);

    void reduceBalance(Long subscriberId, Double delta);

}
