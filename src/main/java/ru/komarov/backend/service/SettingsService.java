package ru.komarov.backend.service;

public interface SettingsService {

    int changeCallLimit(int newCallLimit);

    int getCallLimit();
}
