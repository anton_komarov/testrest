package ru.komarov.backend.service;

import ru.komarov.backend.entity.Subscriber;

public interface SubscriberValidator {

    boolean isActive(Subscriber subscriber);

    boolean isAvailableForCall(Subscriber subscriber);

    boolean isAvailableForSendMessage(Subscriber subscriber);

    void checkBalance(Subscriber subscriber);
}
