package ru.komarov.backend.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.komarov.backend.dto.CallDto;
import ru.komarov.backend.dto.MessageDto;
import ru.komarov.backend.entity.Call;
import ru.komarov.backend.entity.Message;
import ru.komarov.backend.entity.Subscriber;
import ru.komarov.backend.entity.enums.ServiceType;
import ru.komarov.backend.exception.NotExistException;
import ru.komarov.backend.exception.SubscriberBlockedException;
import ru.komarov.backend.repository.CallRepository;
import ru.komarov.backend.repository.CostRepository;
import ru.komarov.backend.repository.MessageRepository;
import ru.komarov.backend.repository.SubscriberRepository;
import ru.komarov.backend.service.SubscriberService;
import ru.komarov.backend.service.SubscriberValidator;

import javax.inject.Inject;

@Slf4j
@Service
public class SubscriberServiceImpl implements SubscriberService {

    private final SubscriberRepository subscriberRepository;
    private final CallRepository callRepository;
    private final MessageRepository messageRepository;
    private final CostRepository costRepository;
    private final SubscriberValidator subscriberValidator;

    @Inject
    public SubscriberServiceImpl(SubscriberRepository subscriberRepository, CallRepository callRepository,
                                 MessageRepository messageRepository, CostRepository costRepository,
                                 SubscriberValidator subscriberValidator) {
        this.subscriberRepository = subscriberRepository;
        this.callRepository = callRepository;
        this.messageRepository = messageRepository;
        this.costRepository = costRepository;
        this.subscriberValidator = subscriberValidator;
    }

    @Override
    public double getBalance(Long id){
        Subscriber subscriber = subscriberRepository.findById(id).orElse(null);
        if (subscriber != null) {
            return subscriber.getBalance();
        }
        throw new NotExistException(id);
    }

    @Override
    @Transactional
    public Call doCall(CallDto callDto) {
        Subscriber fromSubscriber = subscriberRepository.findById(callDto.getFromSubscriberId()).orElse(null);
        if (subscriberValidator.isAvailableForCall(fromSubscriber)) {
            double callCost = costRepository.getActualCostValueByType(ServiceType.CALL);
            double delta = callCost * callDto.getDuration();
            Subscriber toSubscriber = subscriberRepository.findById(callDto.getToSubscriberId()).orElse(null);
            Call newCall = new Call(fromSubscriber, toSubscriber, callDto.getDuration(), callCost);
            reduceBalance(fromSubscriber.getId(), delta);
            return callRepository.save(newCall);
        }
        throw new SubscriberBlockedException(callDto.getFromSubscriberId());
    }

    @Override
    @Transactional
    public Message sendMessage(MessageDto msgDto) {
        Subscriber fromSubscriber = subscriberRepository.findById(msgDto.getFromSubscriberId()).orElse(null);
        if (subscriberValidator.isAvailableForSendMessage(fromSubscriber)) {
            double messageCost = costRepository.getActualCostValueByType(ServiceType.MESSAGE);
            Subscriber toSubscriber = subscriberRepository.findById(msgDto.getToSubscriberId()).orElse(null);
            Message message = new Message(fromSubscriber, toSubscriber, msgDto.getValue(), messageCost);
            reduceBalance(fromSubscriber.getId(), messageCost);
            return messageRepository.save(message);
        }
        throw new SubscriberBlockedException(msgDto.getFromSubscriberId());
    }

    @Override
    public double addToBalance(Long subscriberId, Double delta) {
        return changeBalance(subscriberId, delta);
    }

    @Override
    public void reduceBalance(Long subscriberId, Double delta) {
        changeBalance(subscriberId, -delta);
    }

    private double changeBalance(Long subscriberId, Double delta) {
        Subscriber subscriber = subscriberRepository.findById(subscriberId).orElse(null);
        if (subscriber != null) {
            Double currentBalance = subscriber.getBalance();
            subscriber.setBalance(currentBalance + delta);
            subscriberValidator.checkBalance(subscriber);
            return subscriberRepository.save(subscriber).getBalance();
        }
        throw new NotExistException(subscriberId);
    }
}
