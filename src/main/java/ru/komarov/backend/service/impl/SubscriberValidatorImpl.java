package ru.komarov.backend.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.komarov.backend.entity.Subscriber;
import ru.komarov.backend.entity.enums.SubscriberStatus;
import ru.komarov.backend.exception.BackendException;
import ru.komarov.backend.exception.NotExistException;
import ru.komarov.backend.repository.CallRepository;
import ru.komarov.backend.service.SettingsService;
import ru.komarov.backend.service.SubscriberValidator;

import javax.inject.Inject;

@Slf4j
@Service
public class SubscriberValidatorImpl implements SubscriberValidator {

    private final CallRepository callRepository;
    private final SettingsService settingsService;

    @Inject
    public SubscriberValidatorImpl(CallRepository callRepository,
                                   SettingsService settingsService) {
        this.callRepository = callRepository;
        this.settingsService = settingsService;
    }

    @Override
    public boolean isActive(Subscriber subscriber) {
        if (subscriber != null) {
            checkBalance(subscriber);
            return subscriber.getStatus() == SubscriberStatus.ACTIVE;
        }
        throw new NotExistException();
    }

    @Override
    public boolean isAvailableForCall(Subscriber subscriber) {
        return isActive(subscriber) && isAvailableByCallLimit(subscriber);
    }

    @Override
    public boolean isAvailableForSendMessage(Subscriber subscriber) {
        return isActive(subscriber);
    }

    @Override
    public void checkBalance(Subscriber subscriber) {
        if (subscriber != null) {
            SubscriberStatus oldStatus = subscriber.getStatus();
            SubscriberStatus newStatus = subscriber.getBalance() > 0 ? SubscriberStatus.ACTIVE : SubscriberStatus.BLOCKED;
            if (oldStatus != newStatus) {
                subscriber.setStatus(newStatus);
                LOGGER.info("The subscriber with id='{}' changed his status from '{}' to '{}'", subscriber.getId(), oldStatus, newStatus);
            }
        }
    }

    private boolean isAvailableByCallLimit(Subscriber subscriber) {
        int callLimit = settingsService.getCallLimit();
        boolean isAvailable = callRepository.getNumberOfCallsPerDayForSubscriber(subscriber) < callLimit;
        if (!isAvailable) {
            throw new BackendException(String.format("The maximum number (%d) of calls per day has been reached", callLimit));
        }
        return true;
    }
}
