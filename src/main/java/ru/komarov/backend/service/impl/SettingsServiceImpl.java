package ru.komarov.backend.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.komarov.backend.service.SettingsService;

@Slf4j
@Service
public class SettingsServiceImpl implements SettingsService {

    private int callLimit = 5;

    @Override
    public int changeCallLimit(int newCallLimit) {
        if (newCallLimit >= 0) {
            callLimit = newCallLimit;
        } else {
            LOGGER.warn("Call limit is not changed since it must be greater than zero");
        }
        return callLimit;
    }

    @Override
    public int getCallLimit() {
        return callLimit;
    }
}
