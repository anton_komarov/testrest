package ru.komarov.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.komarov.backend.entity.Subscriber;

public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {
}
