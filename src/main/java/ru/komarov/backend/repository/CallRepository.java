package ru.komarov.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.komarov.backend.entity.Call;
import ru.komarov.backend.entity.Subscriber;

public interface CallRepository extends JpaRepository<Call, Long> {

    @Query("select count(c) from Call c where c.fromSubscriber = :subscriber and c.startTime >= current_date")
    int getNumberOfCallsPerDayForSubscriber(@Param("subscriber") Subscriber subscriber);
}
