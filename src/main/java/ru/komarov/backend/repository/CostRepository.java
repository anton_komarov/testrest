package ru.komarov.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.komarov.backend.entity.Cost;
import ru.komarov.backend.entity.enums.ServiceType;

public interface CostRepository extends JpaRepository<Cost, Long> {

    @Query("select cc.value from Cost cc where cc.id = (select max(c.id) from Cost c where c.serviceType = :serviceType)")
    Double getActualCostValueByType(@Param("serviceType") ServiceType serviceType);
}
