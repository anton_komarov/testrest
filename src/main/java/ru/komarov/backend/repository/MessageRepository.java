package ru.komarov.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.komarov.backend.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
