package ru.komarov.backend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.komarov.backend.dto.ExceptionDto;
import ru.komarov.backend.exception.BackendException;
import ru.komarov.backend.exception.NotExistException;
import ru.komarov.backend.exception.SubscriberBlockedException;

@Slf4j
@ControllerAdvice
public class SpringErrorAdvice {

    @ExceptionHandler(NotExistException.class)
    public ResponseEntity<ExceptionDto> handleNotExistException(NotExistException e) {
        return createResponse(e, NotExistException.ERROR_STATUS);
    }

    @ExceptionHandler(SubscriberBlockedException.class)
    public ResponseEntity<ExceptionDto> handleSubscriberBlockedException(SubscriberBlockedException e) {
        return createResponse(e, SubscriberBlockedException.ERROR_STATUS);
    }

    @ExceptionHandler(BackendException.class)
    public ResponseEntity<ExceptionDto> handleBackendException(BackendException e) {
        return createResponse(e, e.getHttpStatus());
    }

    private ResponseEntity<ExceptionDto> createResponse(RuntimeException e, HttpStatus status) {
        String message = e.getMessage();
        LOGGER.warn(message);
        ExceptionDto exceptionDto = new ExceptionDto(status, message);
        return new ResponseEntity<>(exceptionDto, status);
    }
}
