package ru.komarov.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CallDto {

    private Long id;

    private Long fromSubscriberId;

    private Long toSubscriberId;

    private Long duration;

    private Double cost;

    private LocalDateTime startTime;
}
