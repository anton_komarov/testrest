##Описание REST-функций
####Функция совершения вызова
```shell script
    @PostMapping(value = "calls", consumes = MediaType.APPLICATION_JSON_VALUE)
    public CallDto doCall(@RequestBody CallDto callDto) {
        LOGGER.info("Call doCall() with params: call='{}'", callDto);
        return callConverter.toDto(subscriberService.doCall(callDto));
    }
```
Функция имеет тип HTTP запроса - POST, принимает на вход сущность вызова (CallDto), 
которая находится в формате json, 
создает новый экземпляр класса Call с id звонящего и id принимающего вызов абонента и 
длительностью из входящей сущности, сохраняет его в БД. Результатом является новый 
экземпляр класса CallDto.

####Функция отправки сообщения
```shell script
    @PostMapping(value = "/messages", consumes = MediaType.APPLICATION_JSON_VALUE)
    public MessageDto sendMessage(@RequestBody MessageDto messageDto) {
        LOGGER.info("Call sendMessage() with params: message='{}'", messageDto);
        return messageConverter.toDto(subscriberService.sendMessage(messageDto));
    }
```
Функция имеет тип HTTP запроса - POST, принимает на вход сущность сообщения 
(MessageDto), которая находится в формате json, 
создает новый экземпляр класса Message с id абонента, отправившего сообщение,
id абонента, принимающего сообщение, и содержание из входящей сущности, сохраняет его в БД. 
Результатом является новый экземпляр класса MessageDto.

####Функция проверки баланса
```shell script
    @GetMapping(value = "/subscribers/{id}/balance")
    public Double getBalance(@PathVariable("id") Long id) {
        LOGGER.info("Call getBalance() with params: id='{}'", id);
        return subscriberService.getBalance(id);
    }
```
Функция имеет тип HTTP запроса - GET, принимает на вход id абонента, у которого
необходимо посмотреть баланс. Результом является число типа double.

####Функция пополнения баланса абонента
```shell script
    @PutMapping(value = "/subscribers/{id}/balance", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Double addToBalance(@PathVariable("id") Long id, @RequestBody Double delta) {
        LOGGER.info("Call addToBalance() with params: id='{}', delta='{}'", id, delta);
        return subscriberService.addToBalance(id, delta);
    }
```
Функция имеет тип HTTP запроса - PUT, принимает на вход id абонента,
у которого необходимо пополнить баланс, и величину (тип double), на которую
необходимо изменить баланс (находится в формате json).
Функция прибавляет к текущему балансу введенную величину, сохраняет сумму в БД 
и возвращает результат типа double.

####Функция изменения ограничения на количество звонков в день
```shell script
    @PutMapping(value = "/limit", consumes = MediaType.APPLICATION_JSON_VALUE)
    public int changeCallLimit(@RequestBody int newCallLimit) {
        LOGGER.info("Call changeCallLimit() with params: newCallLimit='{}'", newCallLimit);
        return settingsService.changeCallLimit(newCallLimit);
    }
```
Функция имеет тип HTTP запроса - PUT, принимает на вход число типа integer,
изменяет текущее значение лимита на новое (в случае, если введенное число неотрицательное)
и возвращает текущее значение (тип integer).